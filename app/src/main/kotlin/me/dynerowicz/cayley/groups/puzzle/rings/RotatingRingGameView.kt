package me.dynerowicz.cayley.groups.puzzle.rings

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.util.Property
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.algebra.Group
import me.dynerowicz.cayley.extensions.drawCircle
import me.dynerowicz.cayley.utils.Generator
import me.dynerowicz.cayley.utils.Palette
import me.dynerowicz.cayley.utils.Point
import me.dynerowicz.cayley.views.SymmetryView

class RotatingRingGameView(context: Context, attrs: AttributeSet) : SymmetryView(context, attrs) {

    private val ringAngles: FloatArray
    private val ringAnglesP: Array<RingAngleProperty>
    private val ringStepAngle: Float

    private var ringRadius = 0.0f
    private var markRadius = 0.0f

    private val center = Point()

    init {
        assert(overview is RotatingRingGroupOverviewActivity && group is Group.RotatingRingPuzzleGroup)

        val ringsConfiguration = currentElement as RotatingRingPuzzleElement

        ringAngles = FloatArray(ringsConfiguration.numberOfRings)
        ringAnglesP = Array(ringsConfiguration.numberOfRings) { index -> RingAngleProperty(index) }
        ringStepAngle = (2.0f * Math.PI / ringsConfiguration.numberOfPositions).toFloat()
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        val ringsConfiguration = currentElement as RotatingRingPuzzleElement

        center.set(width / 2.0f, height / 2.0f)
        ringRadius = 0.75f * (Math.min(height, width) / (2 * ringsConfiguration.numberOfRings)).toFloat()
        markRadius = 0.25f * ringRadius
        Log.v(TAG, "onSizeChanged: $center, $ringRadius, $markRadius")
    }

    override fun createAnimator(source: Element, target: Element, generator: Generator): Animator {
        Log.d(TAG, "createAnimator: $source, $target, $generator")
        val generatorRingMoves = generator as RotatingRingPuzzleElement
        val animators = ArrayList<Animator>()

        val switches = source as RotatingRingPuzzleElement

        for (index in 0 until generatorRingMoves.numberOfRings) {
            if (generatorRingMoves[index] == 1) {
                val start = switches[index] * ringStepAngle
                val final = start + ringStepAngle
                animators.add(
                    ObjectAnimator.ofFloat(this, ringAnglesP[index], start, final)
                )
            }
        }

        return AnimatorSet().apply { playTogether(animators) }
    }

    override fun onDraw(canvas: Canvas) {
        Log.v(TAG, "onDraw")
        val palette = overview.paletteActions
        val generators = overview.group.generators
        ringAngles.forEachIndexed { ringIndex, ringAngle ->
            canvas.drawCircle(center, (ringIndex+1) * ringRadius, Palette.WHITE)
            canvas.drawCircle(Point.fromPolar(center, (ringIndex+1) * ringRadius, ringAngle), markRadius, palette[generators[ringIndex]])
        }
    }

    class RingAngleProperty(private val ringIndex: Int) : Property<RotatingRingGameView, Float>(Float::class.java, "Ring@$ringIndex") {

        override fun get(rotatingRings: RotatingRingGameView): Float = rotatingRings.ringAngles[ringIndex]

        override fun set(rotatingRings: RotatingRingGameView, value: Float) {
            rotatingRings.ringAngles[ringIndex] = value
            rotatingRings.invalidate()
        }
    }

    companion object {
        private val TAG = RotatingRingGameView::class.java.simpleName
    }
}