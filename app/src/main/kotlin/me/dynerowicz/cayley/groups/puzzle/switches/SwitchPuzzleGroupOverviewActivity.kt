package me.dynerowicz.cayley.groups.puzzle.switches

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import kotlinx.android.synthetic.main.activity_switch_puzzle_group_overview.*
import me.dynerowicz.cayley.R
import me.dynerowicz.cayley.activity.GroupOverviewActivity
import me.dynerowicz.cayley.views.CayleyDiagramView
import me.dynerowicz.cayley.views.SymmetryView

import me.dynerowicz.cayley.algebra.Group.SwitchPuzzleGroup

class SwitchPuzzleGroupOverviewActivity : GroupOverviewActivity() {
    override lateinit var mainView: ConstraintLayout
    override lateinit var cayleyDiagram: CayleyDiagramView
    override lateinit var symmetryObject: SymmetryView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val order = intent.extras?.getInt(ORDER) ?: 5

        group = SwitchPuzzleGroup(order)

        setContentView(R.layout.activity_switch_puzzle_group_overview)

        mainView = main_view
        cayleyDiagram = cayley_diagram
        symmetryObject = symmetry_object
    }
}