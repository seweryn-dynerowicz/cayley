package me.dynerowicz.cayley.groups.cyclic

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import kotlinx.android.synthetic.main.activity_cyclic_group_overview.*
import me.dynerowicz.cayley.R
import me.dynerowicz.cayley.activity.GroupOverviewActivity
import me.dynerowicz.cayley.views.CayleyDiagramView
import me.dynerowicz.cayley.views.SymmetryView

import me.dynerowicz.cayley.algebra.Group.CyclicGroup

class CyclicGroupOverviewActivity : GroupOverviewActivity() {
    override lateinit var mainView: ConstraintLayout
    override lateinit var cayleyDiagram: CayleyDiagramView
    override lateinit var symmetryObject: SymmetryView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val order = intent.extras?.getInt(ORDER) ?: 6

        group = CyclicGroup(order)

        setContentView(R.layout.activity_cyclic_group_overview)

        mainView = main_view
        cayleyDiagram = cayley_diagram
        symmetryObject = symmetry_object
    }
}