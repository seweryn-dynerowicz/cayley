package me.dynerowicz.cayley.groups.trivial

import me.dynerowicz.cayley.diagram.GraphLayout

import me.dynerowicz.cayley.algebra.Group.TrivialGroup

class TrivialGroupLayout(group: TrivialGroup) : GraphLayout(group) {
    override fun onSizeChanged(height: Int, width: Int) {
        nodesCoordinates.forEach { (_, point) -> point.set(width / 2.0f, height / 2.0f) }
    }
}