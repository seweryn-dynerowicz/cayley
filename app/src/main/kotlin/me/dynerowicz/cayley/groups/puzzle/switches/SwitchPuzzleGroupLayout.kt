package me.dynerowicz.cayley.groups.puzzle.switches

import me.dynerowicz.cayley.diagram.GraphLayout
import me.dynerowicz.cayley.extensions.setEdge
import me.dynerowicz.cayley.utils.Point

import me.dynerowicz.cayley.algebra.Group.SwitchPuzzleGroup

class SwitchPuzzleGroupLayout(group: SwitchPuzzleGroup) : GraphLayout(group) {

    private var center = Point()

    private var innerRadius = 0.0f
    private var outerRadius = 0.0f

    private val thetaFor8: Float = (2.0f * Math.PI / 4).toFloat()

    override fun drawPathCondition(arrow: Arrow): Boolean = arrow.source.id < arrow.target.id

    override fun onSizeChanged(height: Int, width: Int) {
        super.onSizeChanged(height, width)

        center.set(width / 2.0f, height / 2.0f)
        innerRadius = 0.175f * Math.min(height, width)
        outerRadius = 0.425f * Math.min(height, width)

        if (nodesCoordinates.size == 8) {
            nodesCoordinates.forEach { (node, point) ->
                when (node.id) {
                    0 -> point.setFromPolar(center, outerRadius, 0 * thetaFor8)
                    1 -> point.setFromPolar(center, innerRadius, 3 * thetaFor8)
                    2 -> point.setFromPolar(center, innerRadius, 2 * thetaFor8)
                    3 -> point.setFromPolar(center, outerRadius, 1 * thetaFor8)

                    4 -> point.setFromPolar(center, innerRadius, 1 * thetaFor8)
                    5 -> point.setFromPolar(center, outerRadius, 2 * thetaFor8)
                    6 -> point.setFromPolar(center, outerRadius, 3 * thetaFor8)
                    7 -> point.setFromPolar(center, innerRadius, 0 * thetaFor8)
                }
            }

            animationPaths.forEach { (arrow, path) ->
                val source = nodesCoordinates[arrow.source]
                val target = nodesCoordinates[arrow.target]
                path.setEdge(source, target)
            }
        }
    }

    companion object {
        private val TAG = SwitchPuzzleGroupLayout::class.java.simpleName
    }
}