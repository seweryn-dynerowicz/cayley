package me.dynerowicz.cayley.groups.cyclic

import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.extensions.modulo

class CyclicElement(value: Int = 0, val cycleLength: Int = 1) : Element() {
    override val id: Int = value.modulo(cycleLength)

    override fun times(that: Element): Element {
        that as CyclicElement; assert(cycleLength == that.cycleLength)

        return CyclicElement(this.id + that.id, cycleLength)
    }

    override fun not(): Element {
        return CyclicElement(cycleLength - id, cycleLength)
    }
}