package me.dynerowicz.cayley.groups.puzzle.rings

import android.graphics.Path
import android.util.Log
import me.dynerowicz.cayley.algebra.Group.RotatingRingPuzzleGroup
import me.dynerowicz.cayley.diagram.GraphLayout
import me.dynerowicz.cayley.extensions.modulo
import me.dynerowicz.cayley.extensions.setEdge
import me.dynerowicz.cayley.utils.Point

class RotatingRingGroupLayout(group: RotatingRingPuzzleGroup) : GraphLayout(group) {

    private val numberOfRings = group.numberOfRings
    private val numberOfPositions = group.numberOfPositions

    private val centers = Array(numberOfCenters(numberOfRings, numberOfPositions)) { Point() }
    private val radii = FloatArray(group.numberOfRings)
    private val angle = (2.0f * Math.PI / group.numberOfPositions).toFloat()

    override fun drawPathCondition(arrow: Arrow): Boolean {
        val innermost = group.generators.last()
        return arrow.generator == innermost
    }

    override fun onSizeChanged(height: Int, width: Int) {
        super.onSizeChanged(height, width)

        centers[0].set(width / 2.0f, height / 2.0f)

        val minimum = Math.min(width, height)
        for (index in 0 until radii.size)
            radii[index] = minimum / Math.pow(NESTING_SCALE, index + 1.0).toFloat()

        radii.forEachIndexed { index, radius ->
            Log.v(TAG, "Radius[$index] : $radius")
        }

        var base = 0
        for (depth in 0 until numberOfRings) {
            val numberOfCenters = Math.pow(numberOfPositions.toDouble(), depth.toDouble()).toInt()
            for (centerIndex in 0 until numberOfCenters) {
                val currentCenter = centers[base + centerIndex]
                for (nextCenterIndex in 0 until numberOfPositions) {
                    val nextCenter = centers[base + numberOfCenters + centerIndex * numberOfPositions + nextCenterIndex]
                    nextCenter.set(currentCenter)
                    nextCenter.translateByPolar(radii[depth], angle * nextCenterIndex)
                }
            }
            base += numberOfCenters
        }

        nodeRadius *= 0.25f

        nodesCoordinates.forEach { (node, point) ->
            node as RotatingRingPuzzleElement

            // Reset the point to the center
            point.set(centers[0])

            for (depth in 0 until numberOfRings) {
                point.translateByPolar(radii[depth], angle * node[depth])
            }
        }

        base = 0
        for (depth in 0 until numberOfRings) {
            val numberOfCenters = Math.pow(numberOfPositions.toDouble(), depth.toDouble()).toInt()
            for (centerIndex in 0 until numberOfCenters) {
                val currentCenter = centers[base + centerIndex]
                for (nextCenterIndex in 0 until numberOfPositions) {
                    val nextCenter = centers[base + numberOfCenters + centerIndex * numberOfPositions + nextCenterIndex]
                    nextCenter.set(currentCenter)
                    nextCenter.translateByPolar(radii[depth], angle * nextCenterIndex)
                }
            }
            base += numberOfCenters
        }

        Log.d(TAG, "Centers : ${centers.size}")
        centers.forEach { center ->
            Log.d(TAG, "Center @ $center")
        }

        // Take care of all depth levels except the last one which is already handled in the parent class
        var generatorBase = 1
        group.generators.forEachIndexed { depth, generator ->
            if (depth < numberOfRings - 1) {
                val numberOfCycles = Math.pow(numberOfPositions.toDouble(), depth.toDouble()).toInt()
                Log.v(TAG, "Generator $generator [$depth] : $numberOfCycles cycles")
                val generatorPaths: MutableList<Path>? = drawnPaths[generator]
                if (generatorPaths != null) {
                    generatorPaths.clear()
                    for (cycle in 0 until numberOfCycles) {
                        val cycleBase = generatorBase + (cycle * numberOfPositions)
                        Log.v(TAG, "Cycle base = $cycleBase until ${cycleBase + numberOfPositions - 1}")
                        for (center in 0 until numberOfPositions) {
                            val path = Path()
                            path.setEdge(centers[cycleBase + center], centers[cycleBase + (center + 1).modulo(numberOfPositions)])
                            generatorPaths.add(path)
                        }
                    }
                }
                generatorBase += numberOfCycles * numberOfPositions
            }
        }

        animationPaths.forEach { (arrow, path) ->
            val source = nodesCoordinates[arrow.source]
            val target = nodesCoordinates[arrow.target]
            path.setEdge(source, target)
        }
    }

    companion object {
        private val TAG = RotatingRingGroupLayout::class.java.simpleName
        private const val NESTING_SCALE = 3.1

        private fun numberOfCenters(numberOfRings: Int, numberOfPositions: Int) : Int {
            var count = 0.0

            for (level in 0 .. numberOfRings)
                count += Math.pow(numberOfPositions.toDouble(), level.toDouble())

            return count.toInt()
        }
    }
}