package me.dynerowicz.cayley.groups.puzzle.switches

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.util.Property
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.algebra.Group.SwitchPuzzleGroup
import me.dynerowicz.cayley.utils.Generator
import me.dynerowicz.cayley.utils.Paint
import me.dynerowicz.cayley.views.SymmetryView

class SwitchPuzzleGameView(context: Context, attrs: AttributeSet) : SymmetryView(context, attrs) {

    private val switchesC: Array<Paint>
    private val switchesP: Array<SwitchProperty>

    private var switchRadius = 0.0f
    private var centerLine = 0.0f
    private var switchSpacing = 0

    init {
        assert(overview is SwitchPuzzleGroupOverviewActivity && group is SwitchPuzzleGroup)

        val switches = currentElement as SwitchPuzzleElement

        val palette = overview.paletteActions
        val generators = overview.group.generators
        switchesC = Array(switches.numberOfSwitches) { index -> palette[generators[index]].copy().apply { value = 0.2f } }
        switchesP = Array(switches.numberOfSwitches) { index -> SwitchProperty(index) }
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        val switches = currentElement as SwitchPuzzleElement
        centerLine = height / 2.0f
        switchSpacing = width / switches.numberOfSwitches
        switchRadius = 0.15f * (Math.min(height, width) / switches.numberOfSwitches)
    }

    override fun createAnimator(source: Element, target: Element, generator: Generator): Animator {
        Log.d(TAG, "createAnimator: $source, $target, $generator")
        val switches = source as SwitchPuzzleElement
        val generatorSwitches = generator as SwitchPuzzleElement
        val animators = ArrayList<Animator>()

        for (index in 0 until generatorSwitches.numberOfSwitches) {
            if (generatorSwitches.isOn(index)) {
                animators.add(
                    if (switches.isOn(index))
                        ObjectAnimator.ofFloat(this, switchesP[index], 1.0f, 0.2f)
                    else
                        ObjectAnimator.ofFloat(this, switchesP[index], 0.2f, 1.0f)
                )
            }
        }

        return AnimatorSet().apply { playTogether(animators) }
    }

    override fun onDraw(canvas: Canvas) {
        Log.v(TAG, "onDraw")
        group as SwitchPuzzleGroup
        for (index in 0 until group.numberOfSwitches)
            canvas.drawCircle((index + 0.5f) * switchSpacing, centerLine, switchRadius, switchesC[index])
    }

    class SwitchProperty(val id: Int) : Property<SwitchPuzzleGameView, Float>(Float::class.java, "Switch$id") {

        override fun get(switchGame: SwitchPuzzleGameView): Float = switchGame.switchesC[id].value

        override fun set(switchGame: SwitchPuzzleGameView, value: Float) {
            switchGame.switchesC[id].value = value
            switchGame.invalidate()
        }
    }

    companion object {
        private val TAG = SwitchPuzzleGameView::class.java.simpleName
    }
}