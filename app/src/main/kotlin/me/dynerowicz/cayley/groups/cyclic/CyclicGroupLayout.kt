package me.dynerowicz.cayley.groups.cyclic

import android.graphics.RectF
import android.util.Log
import me.dynerowicz.cayley.algebra.Group.CyclicGroup
import me.dynerowicz.cayley.diagram.GraphLayout
import me.dynerowicz.cayley.extensions.radiansToDegrees
import me.dynerowicz.cayley.extensions.setArc
import me.dynerowicz.cayley.utils.Point

class CyclicGroupLayout(group: CyclicGroup) : GraphLayout(group) {

    private var center = Point()
    private var cycleRadius = 0.0f
    private var circle = RectF()

    private val radiusFactor = 0.375f
    private val theta = (2.0f * Math.PI / group.order).toFloat()

    override fun onSizeChanged(height: Int, width: Int) {
        super.onSizeChanged(height, width)

        center = Point(width / 2.0f, height / 2.0f)
        cycleRadius = radiusFactor * Math.min(height, width)
        circle.set(center.x - cycleRadius, center.y - cycleRadius, center.x + cycleRadius, center.y + cycleRadius)

        nodesCoordinates.forEach { (node, point) ->
            point.setFromPolar(center, cycleRadius, node.id * theta)
            Log.d(TAG, "Node: $node@$point")
        }

        animationPaths.forEach { (arrow, path) ->
            Log.d(TAG, "Arrow: ${arrow.source} -> ${arrow.target} [${arrow.generator}]")
            path.setArc(circle, (arrow.source.id * theta + Point.ROTATION).radiansToDegrees(), theta.radiansToDegrees(), arrowHeadLength = 25.0f, arrowHeadOffset = 1.3f * nodeRadius)
        }
    }

    companion object {
        private val TAG = CyclicGroupLayout::class.java.simpleName
    }
}