package me.dynerowicz.cayley.groups.puzzle.rings

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import kotlinx.android.synthetic.main.activity_rotating_rings_group_overview.*
import me.dynerowicz.cayley.R
import me.dynerowicz.cayley.activity.GroupOverviewActivity
import me.dynerowicz.cayley.algebra.Group.RotatingRingPuzzleGroup
import me.dynerowicz.cayley.views.CayleyDiagramView
import me.dynerowicz.cayley.views.SymmetryView

class RotatingRingGroupOverviewActivity : GroupOverviewActivity() {
    override lateinit var mainView: ConstraintLayout
    override lateinit var cayleyDiagram: CayleyDiagramView
    override lateinit var symmetryObject: SymmetryView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val order = intent.extras?.getInt(ORDER) ?: 3

        group = RotatingRingPuzzleGroup(numberOfRings = order, numberOfPositions = 4)

        setContentView(R.layout.activity_rotating_rings_group_overview)

        mainView = main_view
        cayleyDiagram = cayley_diagram
        symmetryObject = symmetry_object
    }
}