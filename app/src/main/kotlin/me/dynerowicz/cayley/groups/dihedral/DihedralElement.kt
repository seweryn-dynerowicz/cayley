package me.dynerowicz.cayley.groups.dihedral

import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.extensions.modulo

class DihedralElement(rotation: Int = 0, val flip: Boolean = false, val numberOfSides: Int = 2) : Element() {
    val rotation: Int = rotation.modulo(numberOfSides)

    override val id: Int = if (!flip) this.rotation else this.rotation + numberOfSides

    override fun times(that: Element): Element {
        that as DihedralElement; assert(numberOfSides == that.numberOfSides)
        return DihedralElement(
                rotation = rotation + if (!flip) that.rotation else -that.rotation,
                flip = this.flip xor that.flip,
                numberOfSides = numberOfSides)
    }

    override fun not(): Element =
            DihedralElement(rotation = if (flip) rotation else -rotation,
                    flip = flip, numberOfSides = numberOfSides)
}