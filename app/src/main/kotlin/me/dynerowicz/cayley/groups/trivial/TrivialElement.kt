package me.dynerowicz.cayley.groups.trivial

import me.dynerowicz.cayley.algebra.Element

object TrivialElement : Element() {
    override val id = 0

    override fun times(that: Element): Element = TrivialElement
    override fun not(): Element = TrivialElement
}