package me.dynerowicz.cayley.groups.dihedral

import android.util.Log
import me.dynerowicz.cayley.utils.Point
import me.dynerowicz.cayley.extensions.setEdge
import me.dynerowicz.cayley.diagram.GraphLayout
import me.dynerowicz.cayley.algebra.Group.DihedralGroup

class DihedralGroupLayout(group: DihedralGroup) : GraphLayout(group) {

    private var center = Point()

    private var innerRadius = 0.0f
    private var outerRadius = 0.0f

    private val theta: Float = (2.0f * Math.PI / group.numberOfSides).toFloat()
    private val iTheta: Float = (((group.numberOfSides - 2) * Math.PI) / (2 * group.numberOfSides)).toFloat()

    override fun drawPathCondition(arrow: Arrow): Boolean {
        arrow.generator as DihedralElement
        return !arrow.generator.flip || arrow.source.id < arrow.target.id
    }

    override fun onSizeChanged(height: Int, width: Int) {
        super.onSizeChanged(height, width)

        center.set(width / 2.0f, height / 2.0f)
        innerRadius = 0.2f * Math.min(height, width)
        outerRadius = 0.425f * Math.min(height, width)

        nodesCoordinates.forEach { (node, point) ->
            node as DihedralElement
            point.setFromPolar(center, if (!node.flip) innerRadius else outerRadius, node.rotation * theta)
        }

        animationPaths.forEach { (arrow, path) ->
            arrow.generator as DihedralElement

            val source = nodesCoordinates[arrow.source]
            val target = nodesCoordinates[arrow.target]

            if (arrow.generator.flip) {
                Log.d(TAG, "Flip: ${arrow.source} -> ${arrow.target} [${arrow.generator}]")
                path.setEdge(source, target)
            } else {
                arrow.source as DihedralElement

                Log.d(TAG, "Rotation: ${arrow.source} -> ${arrow.target} [${arrow.generator.id}]")
                path.setEdge(source, target, angle = iTheta, clockwise = !arrow.source.flip, arrowHeadLength = 25.0f, arrowHeadOffset = 1.3f * nodeRadius)
            }
        }
    }

    companion object {
        private val TAG = DihedralGroupLayout::class.java.simpleName
    }
}