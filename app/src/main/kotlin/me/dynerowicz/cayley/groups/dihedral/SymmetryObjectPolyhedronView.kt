package me.dynerowicz.cayley.groups.dihedral

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.util.AttributeSet
import android.util.Log
import android.util.Property
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.algebra.Group
import me.dynerowicz.cayley.extensions.drawLine
import me.dynerowicz.cayley.extensions.lineTo
import me.dynerowicz.cayley.extensions.moveTo
import me.dynerowicz.cayley.utils.Generator
import me.dynerowicz.cayley.utils.Palette
import me.dynerowicz.cayley.utils.Point
import me.dynerowicz.cayley.views.SymmetryView

class SymmetryObjectPolyhedronView(context: Context, attrs: AttributeSet) : SymmetryView(context, attrs) {
    private var numberOfSides: Int = order / 2

    private var center = Point()
    private var radius = 0.0f
    private var angleStep = (2.0f * Math.PI / numberOfSides).toFloat()

    init {
        assert(overview is DihedralGroupOverviewActivity && group is Group.DihedralGroup)
    }

    private var flip: Float = 1.0f
        set(value) {
            Log.v(TAG, "flip: $value")
            field = value
            camemberts.forEachIndexed { index, camembert ->
                camembert.setFromPolar(center, radius, index, angleStep, offset = - angle, flip = flip)
            }
            invalidate()
        }

    private var angle: Float = 0.0f
        set(value) {
            Log.v(TAG, "angle: $value")
            field = value
            camemberts.forEachIndexed { index, camembert ->
                camembert.setFromPolar(center, radius, index, angleStep, offset = - angle, flip = flip)
            }
            invalidate()
        }

    private var camemberts: Array<Camembert> = Array(numberOfSides) { Camembert(center, radius, it, angleStep) }

    override fun createAnimator(source: Element, target: Element, generator: Generator): Animator {
        source as DihedralElement
        target as DihedralElement
        generator as DihedralElement

        return if(generator.flip) {
            if (source.flip)
                ObjectAnimator.ofFloat(this, Flip, -1.0f, 1.0f)
            else
                ObjectAnimator.ofFloat(this, Flip, 1.0f, -1.0f)
        } else {
            val sourceAngle = (if (source.id == numberOfSides) 2 * numberOfSides else source.id) * angleStep
            val targetAngle = (if (target.id == 0) numberOfSides else target.id) * angleStep

            ObjectAnimator.ofFloat(this, Angle, sourceAngle, targetAngle)
        }
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        center.set(width / 2.0f, height / 2.0f)
        radius = 0.35f * Math.min(height, width)

        camemberts.forEachIndexed { index, camembert -> camembert.setFromPolar(center, radius, index, angleStep) }
    }

    override fun onDraw(canvas: Canvas) {
        // Draw the camemberts in the polygon
        val palette = overview.paletteElements
        camemberts.forEachIndexed { index, camembert ->
            val derivedIndex = if (flip > 0.0f) index else (index + numberOfSides)

            canvas.drawPath(camembert.path, palette[group.elements[derivedIndex]])
            canvas.drawLine(camembert.summitLeft, camembert.summitRight, Palette.WHITE)
        }
    }

    object Flip : Property<SymmetryObjectPolyhedronView, Float>(Float::class.java, "Flip") {
        override fun get(polyhedron: SymmetryObjectPolyhedronView): Float = polyhedron.flip
        override fun set(polyhedron: SymmetryObjectPolyhedronView, value: Float) { polyhedron.flip = value }
    }

    object Angle : Property<SymmetryObjectPolyhedronView, Float>(Float::class.java, "Angle") {
        override fun get(polyhedron: SymmetryObjectPolyhedronView): Float = polyhedron.angle
        override fun set(polyhedron: SymmetryObjectPolyhedronView, value: Float) { polyhedron.angle = value }
    }

    class Camembert(center: Point, radius: Float, index: Int, theta: Float, offset: Float = 0.0f) {
        var summitLeft = Point()
            private set
        var summitRight = Point()
            private set

        val path: Path = Path()

        init { setFromPolar(center, radius, index, theta, offset) }

        fun setFromPolar(center: Point, radius: Float, index: Int, theta: Float, offset: Float = 0.0f, flip: Float = 1.0f) {
            val alpha: Double = (Math.PI / 2.0f) - (theta / 2.0f)
            val height: Float = (radius * Math.sin(alpha)).toFloat()

            Log.v(TAG, "index=$index, theta=$theta, radius=$radius, alpha=$alpha, height=$height, center=$center, safety check = ${(theta / 2.0f) + alpha}=${Math.PI / 2.0f}")

            summitLeft.setFromPolar(center, radius, (index - 0.5f) * theta + offset)
            summitRight.setFromPolar(center, radius, (index + 0.5f) * theta + offset)

            summitLeft.x = flip * (summitLeft.x - center.x) + center.x
            summitRight.x = flip * (summitRight.x - center.x) + center.x

            path.run {
                reset()
                // Center point
                moveTo(center)
                // Line from center to left
                lineTo(summitLeft)
                // Line from left to right
                lineTo(summitRight)
                // Close the path
                close()
            }
        }
    }

    companion object {
        private val TAG = SymmetryObjectPolyhedronView::class.java.simpleName
    }
}