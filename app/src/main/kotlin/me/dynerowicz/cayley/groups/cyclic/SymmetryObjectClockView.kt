package me.dynerowicz.cayley.groups.cyclic

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.util.Property
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.algebra.Group
import me.dynerowicz.cayley.extensions.drawCircle
import me.dynerowicz.cayley.extensions.drawLine
import me.dynerowicz.cayley.utils.Generator
import me.dynerowicz.cayley.utils.Palette
import me.dynerowicz.cayley.utils.Point
import me.dynerowicz.cayley.views.SymmetryView

class SymmetryObjectClockView(context: Context, attrs: AttributeSet) : SymmetryView(context, attrs) {
    private var markers: MutableMap<Element, Marker> = HashMap()

    private var center = Point()
    private var clockRadius = 0.0f

    private var markerAngle: Float = (2.0f * Math.PI / overview.group.order).toFloat()

    private var handAngle: Float = 0.0f
        set(value) {
            field = value
            handPosition.setFromPolar(center, CLOCK_HAND_FACTOR * clockRadius, handAngle)
            invalidate()
        }
    private var handPosition = Point()

    init {
        assert(overview is CyclicGroupOverviewActivity && group is Group.CyclicGroup)

        group.elements.forEach { markers.put(it, Marker(center, clockRadius, it.id * markerAngle)) }
    }

    override fun createAnimator(source: Element, target: Element, generator: Generator): Animator {
        val sourceAngle = source.id * markerAngle
        val targetAngle = markerAngle * if (source.id <= target.id) target.id else (target.id + markers.size)

        Log.i(TAG, "Animator from ${source.id} ($sourceAngle) to ${target.id} ($targetAngle)")
        return ObjectAnimator.ofFloat(this, HandAngle, sourceAngle, targetAngle)
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)

        center.set(width / 2.0f, height / 2.0f)
        clockRadius = 0.375f * Math.min(height, width)

        handPosition.setFromPolar(center, CLOCK_HAND_FACTOR * clockRadius, handAngle)

        markers.forEach { (state, marker) ->
            marker.setFromPolar(center, clockRadius, state.id * markerAngle)
        }

        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        Log.v(TAG, "Hand position: $handPosition")

        // Draw the face of the clock
        canvas.drawCircle(center, clockRadius, Palette.WHITE)

        // Draw the clock handPosition
        canvas.drawLine(center, handPosition, Palette.WHITE)

        // Draw the markers around the face of the clock
        val palette = overview.paletteElements
        markers.forEach { (state, marker) ->
            canvas.drawLine(marker.start, marker.final, palette[state])
        }
    }

    class Marker(center: Point, clockRadius: Float, angle: Float) {
        val start: Point = Point.fromPolar(center, CLOCK_MARKER_START_RADIUS * clockRadius, angle)
        val final: Point = Point.fromPolar(center, CLOCK_MARKER_FINAL_RADIUS * clockRadius, angle)

        fun setFromPolar(center: Point, clockRadius: Float, angle: Float) {
            start.setFromPolar(center, CLOCK_MARKER_START_RADIUS * clockRadius, angle)
            final.setFromPolar(center, CLOCK_MARKER_FINAL_RADIUS * clockRadius, angle)
        }
    }

    object HandAngle : Property<SymmetryObjectClockView, Float>(Float::class.java, "HandAngle") {
        override fun get(clock: SymmetryObjectClockView): Float = clock.handAngle
        override fun set(clock: SymmetryObjectClockView, value: Float) { clock.handAngle = value }
    }

    companion object {
        private val TAG = SymmetryObjectClockView::class.java.simpleName

        private val CLOCK_MARKER_START_RADIUS = 0.725f
        private val CLOCK_MARKER_FINAL_RADIUS = 0.925f
        private val CLOCK_HAND_FACTOR = 0.65f
    }
}