package me.dynerowicz.cayley.groups.puzzle.switches

import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.extensions.add
import me.dynerowicz.cayley.groups.puzzle.rings.RotatingRingPuzzleElement

class SwitchPuzzleElement(switches: IntArray) : Element() {
    private constructor(state: RotatingRingPuzzleElement) : this(state.rings)

    constructor(numberOfSwitches: Int) : this(IntArray(numberOfSwitches))

    constructor(pressedSwitch: Int, numberOfSwitches: Int) : this(
        IntArray(numberOfSwitches) {
            when (it) {
                in (pressedSwitch-1) .. (pressedSwitch+1) -> 1
                else -> 0
            }
        }
    )

    private val state = RotatingRingPuzzleElement(rings = switches, numberOfPositions = 2)
    val numberOfSwitches = state.numberOfRings

    override val id: Int = state.id

    override fun not(): Element = SwitchPuzzleElement(!state as RotatingRingPuzzleElement)

    override fun times(that: Element): Element {
        that as SwitchPuzzleElement; assert(numberOfSwitches == that.numberOfSwitches)

        return SwitchPuzzleElement((state * that.state) as RotatingRingPuzzleElement)
    }

    fun isOn(position: Int) = state[position] == 1
}