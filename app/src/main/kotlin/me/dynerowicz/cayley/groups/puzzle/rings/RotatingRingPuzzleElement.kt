package me.dynerowicz.cayley.groups.puzzle.rings

import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.extensions.add
import me.dynerowicz.cayley.extensions.invert
import me.dynerowicz.cayley.extensions.toDigitString
import me.dynerowicz.cayley.extensions.toInt

open class RotatingRingPuzzleElement(val rings: IntArray, val numberOfPositions: Int) : Element() {
    constructor(numberOfRings: Int, numberOfPositions: Int) : this(IntArray(numberOfRings), numberOfPositions)

    override val id = rings.toInt(radix = numberOfPositions)

    val numberOfRings = rings.size
    protected val size = rings.size

    operator fun get(position: Int): Int = rings[position]

    override fun times(that: Element): Element {
        that as RotatingRingPuzzleElement; assert(size == that.size)

        return RotatingRingPuzzleElement(
            numberOfPositions = numberOfPositions,
            rings = rings.copyOf().add(that.rings, divisor = numberOfPositions)
        )
    }

    override fun not(): Element = RotatingRingPuzzleElement(numberOfPositions = numberOfPositions, rings = rings.copyOf().invert(numberOfPositions))

    override fun toString(): String = rings.toDigitString()
}