package me.dynerowicz.cayley.views

import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import me.dynerowicz.cayley.activity.GroupOverviewActivity
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.utils.Generator

abstract class SymmetryView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {
    protected val overview = context as GroupOverviewActivity
    protected val group = overview.group
    protected val order = group.order

    protected var currentElement = group.neutral
        private set

    fun apply(generators: List<Generator>): List<Animator> {
        Log.v(TAG, "Applying : $generators")
        val animators = ArrayList<Animator>()

        generators.forEach { generator ->
            val nextElement = generator.actingOn(currentElement)
            animators.add(createAnimator(currentElement, nextElement, generator))
            currentElement = nextElement
        }

        return animators
    }

    abstract fun createAnimator(source: Element, target: Element, generator: Generator): Animator

    companion object {
        private val TAG = SymmetryView::class.java.simpleName
    }
}