package me.dynerowicz.cayley.views

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.View
import me.dynerowicz.cayley.algebra.Group

import me.dynerowicz.cayley.extensions.drawTitleBox

import me.dynerowicz.cayley.algebra.Group.TrivialGroup

class CycleGraphView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {

    var group: Group = TrivialGroup

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        Log.v(TAG, "onSizeChanged($width, $height, $oldWidth, $oldHeight")
        invalidate()
    }

    override fun onDraw(canvas: Canvas) = canvas.drawTitleBox(width, height, "Cycle Graph")

    companion object {
        private val TAG = CycleGraphView::class.java.simpleName
    }
}