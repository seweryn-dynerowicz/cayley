package me.dynerowicz.cayley.views

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.PathMeasure
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.util.Property
import android.view.MotionEvent
import android.view.View
import me.dynerowicz.cayley.activity.GroupOverviewActivity
import me.dynerowicz.cayley.algebra.*
import me.dynerowicz.cayley.diagram.GraphLayout
import me.dynerowicz.cayley.utils.Point
import me.dynerowicz.cayley.extensions.drawHighlight
import me.dynerowicz.cayley.extensions.drawNode
import me.dynerowicz.cayley.extensions.set
import me.dynerowicz.cayley.utils.Generator

class CayleyDiagramView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {

    private val overview: GroupOverviewActivity = context as GroupOverviewActivity
    private val group: Group = overview.group
    private val layout: GraphLayout = GraphLayout.getLayout(group)

    private var currentNode: Element = group.neutral
    private var highlight: Point = layout.nodesCoordinates[currentNode]?.copy() ?: Point()

    private val touchRadiusFactor: Float = 2.0f
    private var nodeRadius: Float = 0.0f

    private val dirty = Rect()

    private var arrows = ArrayList<PathMeasure>()
    var arrowProgress: Float = 0.0f
        set(value) {
            field = value
            if(arrows.isNotEmpty()) {
                val coordinates = FloatArray(2)
                val current = arrows.first()
                Log.d(TAG, "length: ${arrowProgress * current.length} / ${current.length}")
                current.getPosTan(arrowProgress * current.length, coordinates, null)
                if (arrowProgress == 1.0f)
                    arrows.removeAt(0)
                dirty.set(highlight, coordinates, boundary = nodeRadius + 10)
                highlight.set(coordinates)
                invalidate(dirty)
            }
        }

    fun apply(generators: List<Generator>): List<Animator> {
        val animators = ArrayList<Animator>()

        generators.forEach { generator ->
            val nextNode = generator.actingOn(currentNode)
            val path = layout.getAnimationPath(currentNode, nextNode, generator)
            Log.i(TAG, "Apply: $currentNode -> $nextNode [$generator] : $path")
            if (path != null) {
                arrows.add(PathMeasure(path, false))
                animators.add(ObjectAnimator.ofFloat(this, CayleyDiagramView.PathProgress, 0.0f, 1.0f))
                currentNode = nextNode
            } else
                Log.e(TAG, "No path found to represent given arrow: $currentNode -> $nextNode")
        }

        return animators
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        layout.onSizeChanged(measuredHeight, measuredWidth)
        nodeRadius = layout.nodeRadius
        highlight = layout.nodesCoordinates[currentNode]?.copy() ?: Point()
        Log.d(TAG, "New node radius: $nodeRadius")
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        // TODO: improve this computation to determine the position ...
        if (event.action == MotionEvent.ACTION_UP) {
            val touchCoordinates = Point(event.x, event.y)

            Log.v(TAG, "Touch at : $touchCoordinates [nodesCoordinates.size=$group.order]")
            layout.nodesCoordinates.forEach { (node, point) ->
                Log.v(TAG, "$node@$point : ${touchCoordinates.distance(point)} <= ${touchRadiusFactor * nodeRadius}")
                if (touchCoordinates.distance(point) <= touchRadiusFactor * nodeRadius)
                    overview.onTappedElement(node)
            }
        }

        return true
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        Log.v(TAG, "onDraw: order (${group.order}), layout details : $layout")
        Log.v(TAG, "isHardwareAccelerated = ${canvas.isHardwareAccelerated}")

        val paletteActions = overview.paletteActions
        val paletteElements = overview.paletteElements

        layout.drawnPaths.forEach { (generator, paths) ->
            paths.forEach { path ->
                Log.v(TAG, "drawArrow : ($generator), ${paletteActions[generator].strokeWidth}")
                canvas.drawPath(path, paletteActions[generator])
            }
        }

        layout.nodesCoordinates.forEach { (node, point) ->
            Log.v(TAG, "drawNode : $node ($node) @ $point")
            canvas.drawNode(node, point, nodeRadius, paletteElements[node])
        }

        canvas.drawHighlight(highlight, nodeRadius)
    }

    object PathProgress : Property<CayleyDiagramView, Float>(Float::class.java, "PathProgress") {
        override fun get(diagram: CayleyDiagramView): Float = diagram.arrowProgress
        override fun set(diagram: CayleyDiagramView, value: Float) { diagram.arrowProgress = value }
    }

    companion object {
        private val TAG = CayleyDiagramView::class.java.simpleName
        // TODO: find the clean way to offset the baseline to center the names vertically in their nodes
    }
}
