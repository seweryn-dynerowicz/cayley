package me.dynerowicz.cayley.views

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.View
import me.dynerowicz.cayley.activity.GroupOverviewActivity
import me.dynerowicz.cayley.algebra.MultiplicationTable
import me.dynerowicz.cayley.extensions.Dimension
import me.dynerowicz.cayley.extensions.drawBox
import me.dynerowicz.cayley.extensions.drawCell

class MultiplicationTableView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {
    private val overview = context as GroupOverviewActivity

    private val order = overview.group.order
    private val table = MultiplicationTable(overview.group)
    private val palette = overview.paletteElements

    private var cellSize = Dimension()
    private var offset = Dimension()

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        cellSize.set(width / order, height / order)
        offset.set((width - (cellSize.x * order)) / 2, (height - (cellSize.y * order)) / 2)
        Log.i(TAG, "onSizeChanged($width, $height, $oldWidth, $oldHeight)")
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        Log.i(TAG, "Drawing multiplication table of order : $order [cellSize=$cellSize]")
        for (row in 0 until order)
            for (col in 0 until order)
                canvas.drawCell(row, col, cellSize, offset, palette[table[row * order + col]])
    }

    companion object {
        private val TAG = MultiplicationTableView::class.java.simpleName
    }
}
