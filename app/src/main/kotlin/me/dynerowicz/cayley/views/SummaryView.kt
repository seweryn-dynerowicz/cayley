package me.dynerowicz.cayley.views

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.View

import me.dynerowicz.cayley.extensions.drawTitleBox

class SummaryView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {
    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        Log.v(TAG, "onSizeChanged($width, $height, $oldWidth, $oldHeight)")
        Log.v(TAG, "onSizeChanged : ")
        invalidate()
    }

    override fun onDraw(canvas: Canvas) = canvas.drawTitleBox(width, height,"Summary/Controls")

    companion object {
        private val TAG = SummaryView::class.java.simpleName
    }
}