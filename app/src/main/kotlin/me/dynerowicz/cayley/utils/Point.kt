package me.dynerowicz.cayley.utils

import java.lang.Math

data class Point(var x: Float = 0.0f, var y: Float = 0.0f) {
    constructor(coordinates: FloatArray) : this(coordinates[0], coordinates[1])

    operator fun plus(that: Point) = Point(this.x + that.x, this.y + that.y)
    operator fun minus(that: Point) = Point(this.x - that.x, this.y - that.y)

    fun scale(sc: Float): Point {
        x *= sc
        y *= sc
        return this
    }

    fun normalize(): Point {
        val length = Math.sqrt((x*x + y*y).toDouble()).toFloat()
        if (length > 0.0f) {
            x /= length
            y /= length
        }
        return this
    }

    fun rotate(theta: Double): Point {
        val xRotated = x * Math.cos(theta) - y * Math.sin(theta)
        val yRotated = x * Math.sin(theta) + y * Math.cos(theta)
        x = xRotated.toFloat()
        y = yRotated.toFloat()
        return this
    }

    fun set(center: Point) {
        x = center.x
        y = center.y
    }

    fun set(coordinates: FloatArray) {
        x = coordinates[0]
        y = coordinates[1]
    }

    fun set(nx: Float, ny: Float) {
        x = nx
        y = ny
    }

    fun set(nx: Int, ny: Int) {
        x = nx.toFloat()
        y = ny.toFloat()
    }

    fun distance(that: Point): Float {
        val dX = that.x - this.x
        val dY = that.y - this.y

        return Math.sqrt((dX * dX + dY * dY).toDouble()).toFloat()
    }

    fun setFromPolar(center: Point, radius: Float, theta: Float) {
        x = xFromPolar(center, radius, theta)
        y = yFromPolar(center, radius, theta)
    }

    fun translateByPolar(radius: Float, theta: Float) {
        x += xFromPolar(radius, theta)
        y += yFromPolar(radius, theta)
    }

    companion object {
        val ROTATION = (-Math.PI).toFloat() / 2.0f

        private fun xFromPolar(radius: Float, theta: Float): Float =
                radius * Math.cos((theta + ROTATION).toDouble()).toFloat()

        fun xFromPolar(center: Point, radius: Float, theta: Float): Float =
            center.x + xFromPolar(radius, theta)

        private fun yFromPolar(radius: Float, theta: Float): Float =
                radius * Math.sin((theta + ROTATION).toDouble()).toFloat()

        fun yFromPolar(center: Point, radius: Float, theta: Float): Float =
            center.y + yFromPolar(radius, theta)

        private fun xFromPolar(center: Point, radius: Int, theta: Float): Float =
                center.x + radius * Math.cos((theta + ROTATION).toDouble()).toFloat()

        private fun yFromPolar(center: Point, radius: Int, theta: Float): Float =
                center.y + radius * Math.sin((theta + ROTATION).toDouble()).toFloat()

        fun fromPolar(center: Point, radius: Float, theta: Float): Point =
                Point(xFromPolar(center, radius, theta), yFromPolar(center, radius, theta))

        fun fromPolar(center: Point, radius: Int, theta: Float): Point =
                Point(xFromPolar(center, radius, theta), yFromPolar(center, radius, theta))

        fun toAngle(center: Point, point: Point): Float =
                Math.atan2((point.y - center.y).toDouble(), (point.x - center.x).toDouble()).toFloat() - ROTATION

        fun toRadius(center: Point, point: Point): Float =
                center.distance(point)
    }
}
