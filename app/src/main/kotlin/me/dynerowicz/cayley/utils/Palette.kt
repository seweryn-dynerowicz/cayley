package me.dynerowicz.cayley.utils

import android.graphics.Paint.Style
import android.util.Log
import me.dynerowicz.cayley.algebra.Element

class Palette private constructor() {

    constructor(elements: MutableList<Element>,
        saturation: Float = 0.6f, value: Float = 0.8f,
        style: Style = Style.STROKE, strokeWidth: Float = 1.0f,
        textSize: Float = 24.0f
    ) : this() {
        val hueDistance = 360.0f / elements.size
        elements.forEachIndexed { index, element ->
            colorPalette[element] = Paint(index * hueDistance, saturation, value, style, strokeWidth, textSize)
        }
    }

    constructor(elements: Array<out Element>,
        saturation: Float = 0.6f, value: Float = 0.8f,
        style: Style = Style.STROKE, strokeWidth: Float = 1.0f,
        textSize: Float = 24.0f
    ) : this() {
        val hueDistance = 360.0f / elements.size
        elements.forEachIndexed { index, element ->
            colorPalette[element] = Paint(index * hueDistance, saturation, value, style, strokeWidth, textSize)
        }
    }

    private val TAG: String = Palette::class.java.simpleName

    private val colorPalette: MutableMap<Element, Paint> = HashMap()

    operator fun get(elt: Element) : Paint {
        var paint = colorPalette[elt]

        if (paint == null) {
            Log.e(TAG, "Requested paint for unknown element : $elt")
            paint = Palette.WHITE
        }

        return paint
    }

    companion object {
        val BLACK = Paint(hue = 0.0f, saturation = 0.0f, value = 0.0f, style = Style.FILL, strokeWidth = 1.0f, textSize = 24.0f)
        val WHITE = Paint(hue = 0.0f, saturation = 0.0f, value = 1.0f, style = Style.STROKE, strokeWidth = 2.0f, textSize = 36.0f)
        val WHITE_FILL = Paint(hue = 0.0f, saturation = 0.0f, value = 1.0f, style = Style.FILL, strokeWidth = 2.0f, textSize = 36.0f)
        val GRAY = Paint(hue = 0.0f, saturation = 0.0f, value = 0.33f, style = Style.FILL, strokeWidth = 1.0f, textSize = 36.0f)
        val HIGHLIGHT = Paint(hue = 0.0f, saturation = 0.0f, value = 1.0f, style = Style.STROKE, strokeWidth = 4.0f, textSize = 36.0f)
    }
}