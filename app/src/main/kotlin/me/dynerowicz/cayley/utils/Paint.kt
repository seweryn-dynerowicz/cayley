package me.dynerowicz.cayley.utils

import android.graphics.Paint

class Paint(private val hue: Float, private val saturation: Float, value: Float,
            style: Paint.Style, strokeWidth: Float, textSize: Float
) : Paint() {

    var value = value
        set(newValue) {
            field = newValue
            color = hsv2rgb(hue, saturation, value)
        }

    init {
        color = hsv2rgb(hue, saturation, value)
        this.style = style
        this.strokeWidth = strokeWidth
        this.textSize = textSize
        textAlign = Paint.Align.CENTER
        isAntiAlias = true
    }

    fun copy() = Paint(hue, saturation, value, style, strokeWidth, textSize)

    companion object {
        private fun hsv2rgb(hue: Float, saturation: Float, value: Float): Int {
            val chroma: Float = value * saturation
            val hPrime: Float = hue / 60.0f
            val X: Float = chroma * (1 - Math.abs(hPrime % 2 - 1))
            val m = value - chroma

            val (red, green, blue) = when(hue) {
                in   0 until  60 -> Triple(chroma, X, 0.0f)
                in  60 until 120 -> Triple(X, chroma, 0.0f)
                in 120 until 180 -> Triple(0.0f, chroma, X)
                in 180 until 240 -> Triple(0.0f, X, chroma)
                in 240 until 300 -> Triple(X, 0.0f, chroma)
            // When hue in 300 until 360
                else -> Triple(chroma, 0.0f, X)
            }

            var rgbColor = -0x1000000
            rgbColor = rgbColor or (((red + m) * 255.0f).toInt() shl 16)
            rgbColor = rgbColor or (((green + m) * 255.0f).toInt() shl 8)
            rgbColor = rgbColor or ((blue + m) * 255.0f).toInt()

            return rgbColor
        }
    }
}