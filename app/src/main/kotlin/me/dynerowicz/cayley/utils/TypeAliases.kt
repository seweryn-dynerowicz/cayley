package me.dynerowicz.cayley.utils

import me.dynerowicz.cayley.algebra.Element

typealias Parent = Element
typealias Generator = Element
typealias PrecedenceInfo = Pair<Parent, Generator>