package me.dynerowicz.cayley.utils

class Animation {
    enum class Result {
        HasMore,
        EndOfStage,
        EndOfAnimation
    }

    companion object Constants {
        val durationPerPath = 100L
        val framesPerPath = 10
    }
}