package me.dynerowicz.cayley.extensions

fun IntArray.toDigitString() : String =
    StringBuilder().apply {
        this@toDigitString.forEach { digit -> append(digit) }
    }.toString()

fun IntArray.toInt(radix: Int) : Int {
    var value = 0
    forEachIndexed { index, digit ->
        value += digit * Math.pow(radix.toDouble(), index.toDouble()).toInt()
    }
    return value
}

fun IntArray.add(that: IntArray, divisor: Int) : IntArray {
    assert(size == that.size)

    for (index in 0 until size)
        this[index] = (this[index] + that[index]).modulo(divisor)

    return this
}

fun IntArray.invert(divisor: Int) : IntArray {
    for (index in 0 until size)
        this[index] = (- this[index]).modulo(divisor)

    return this
}