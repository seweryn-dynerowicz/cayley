package me.dynerowicz.cayley.extensions

// Needed because the default 'modulo' operator misbehaves with negative numbers.
fun Int.modulo(n: Int) = if (this < 0) this + n else rem(n)

fun Int.divisors() =
    List(Math.sqrt(toDouble()).toInt(), { it } )
        .filter { ((it+1).rem(this) == 0) }

fun Int.quotients() = divisors().map { this / it }

fun Float.radiansToDegrees(): Float = ((this * 180.0f) / Math.PI).toFloat()
fun Float.degreesToRadians(): Float = ((this * Math.PI) / 180.0f).toFloat()