package me.dynerowicz.cayley.extensions

import android.graphics.*
import android.util.Log
import me.dynerowicz.cayley.utils.Point

private const val TAG = "Path"

fun Path.setArc(circle: RectF, sourceAngle: Float, sweepAngle: Float, arrowHeadOffset: Float = 0.0f, arrowHeadLength: Float = 0.0f) {
    Log.d(TAG, "CircleArc: $sourceAngle -> $sweepAngle [circle=$circle]")
    reset()
    arcTo(circle, sourceAngle, sweepAngle)

    if (arrowHeadLength != 0.0f) {
        val pathMeasure = PathMeasure(this, false)

        val coordinates = FloatArray(2)
        pathMeasure.getPosTan(pathMeasure.length - arrowHeadOffset, coordinates, null)
        val arrowTip = Point(coordinates)

        pathMeasure.getPosTan(pathMeasure.length - arrowHeadOffset - arrowHeadLength, coordinates, null)
        val arrowBase = Point(coordinates)

        val normal = (arrowTip - arrowBase).normalize()

        val arrowBaseLeftPoint = arrowBase + normal.copy().scale(5.0f).rotate(Math.PI / 2L)
        val arrowBaseRightPoint = arrowBase + normal.copy().scale(5.0f).rotate(- Math.PI / 2L)

        moveTo(arrowTip)
        lineTo(arrowBaseLeftPoint)
        lineTo(arrowBaseRightPoint)
        lineTo(arrowTip)
    }
}

fun Path.setEdge(source: Point?, target: Point?, angle: Float = 0.0f, clockwise: Boolean = true, arrowHeadOffset: Float = 0.0f, arrowHeadLength: Float = 0.0f) {
    if (source != null && target != null) {
        Log.d(TAG, "Edge: ${source.distance(target)}")
        val theta = 0.75 * ((Math.PI / 2L) - angle)
        val distance = source.distance(target) / 2L
        val baseAngle = Point.toAngle(source, target)
        val middle = Point.fromPolar(source, distance / Math.cos(theta).toFloat(), baseAngle + if (clockwise) -theta.toFloat() else theta.toFloat())

        Log.d(TAG, "Edge: $source -> $middle -> $target [theta=$theta, distance=$distance]")

        reset()
        moveTo(source)
        lineTo(middle)
        lineTo(target)

        if (arrowHeadLength != 0.0f) {
            val length = middle.distance(target) - arrowHeadOffset
            val normal = (target - middle).normalize()
            val point1 = middle + normal.copy().scale(length - arrowHeadLength) + normal.copy().scale(5.0f).rotate(Math.PI / 2L)
            val point2 = middle + normal.copy().scale(length)
            val point3 = middle + normal.copy().scale(length - arrowHeadLength) + normal.copy().scale(5.0f).rotate(- Math.PI / 2L)

            moveTo(point2)
            lineTo(point1)
            lineTo(point3)
            lineTo(point2)
        }

        close()
    }
}

fun Path.setEdge(source: Point?, target: Point?) {
    Log.d(TAG, "Edge: $source -> $target")
    if (source != null && target != null) {
        reset()
        moveTo(source)
        lineTo(target)
    }
}

fun Path.moveTo(point: Point) { moveTo(point.x, point.y) }

fun Path.lineTo(point: Point) { lineTo(point.x, point.y) }

fun Path.moveTo(center: Point = Point(), radius: Float, angle: Float) {
    moveTo(Point.xFromPolar(center, radius, angle), Point.yFromPolar(center, radius, angle))
}

fun Path.lineTo(center: Point = Point(), radius: Float, angle: Float) {
    lineTo(Point.xFromPolar(center, radius, angle), Point.yFromPolar(center, radius, angle))
}