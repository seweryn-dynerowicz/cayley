package me.dynerowicz.cayley.extensions

import android.graphics.Rect
import me.dynerowicz.cayley.utils.Point

fun Rect.set(oldPoint: Point, newPoint: FloatArray, boundary: Float) {
    val left : Int = (Math.min(oldPoint.x, newPoint[0]) - boundary).toInt()
    val right : Int = (Math.max(oldPoint.x, newPoint[0]) + boundary).toInt()

    val top : Int = (Math.min(oldPoint.y, newPoint[1]) - boundary).toInt()
    val bottom : Int = (Math.max(oldPoint.y, newPoint[1]) + boundary).toInt()

    set(left, top, right, bottom)
}
