package me.dynerowicz.cayley.extensions

import android.util.Log
import android.view.View
import me.dynerowicz.cayley.utils.Point

fun View.invalidate(point1: Point, point2: Point) {
    val left = minOf(point1.x, point2.x).toInt()
    val right = maxOf(point1.x, point2.x).toInt()
    val top = minOf(point1.y, point2.y).toInt()
    val bottom = maxOf(point1.y, point2.y).toInt()
    Log.v("View", "invalidate: $left, $top, $right, $bottom")
    invalidate(left, top, right, bottom)
}