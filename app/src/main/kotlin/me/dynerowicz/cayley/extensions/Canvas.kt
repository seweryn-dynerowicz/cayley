package me.dynerowicz.cayley.extensions

import android.graphics.*
import android.util.Log
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.utils.Palette
import me.dynerowicz.cayley.utils.Point

const val BORDER_WIDTH = 5.0f

typealias Dimension = Point

private val tag = Canvas::class.java.simpleName

fun Canvas.drawTitleBox(width: Int, height: Int, title: String) {
    Log.v(tag, "drawTitleBox : $title within [$width x $height]")
    drawText(title, width / 2.0f, height / 2.0f, Palette.WHITE_FILL)
    drawRect(BORDER_WIDTH, BORDER_WIDTH, width - BORDER_WIDTH, height - BORDER_WIDTH, Palette.WHITE)
}

fun Canvas.drawBox() {
    drawRect(BORDER_WIDTH, BORDER_WIDTH, width - BORDER_WIDTH, height - BORDER_WIDTH, Palette.WHITE)
}

fun Canvas.drawLine(start: Point, final: Point, paint: Paint) =
    drawLine(start.x, start.y, final.x, final.y, paint)

fun Canvas.drawCircle(center: Point, radius: Float, paint: Paint) =
    drawCircle(center.x, center.y, radius, paint)

val baselineOffset = 8.0f
fun Canvas.drawNode(node: Element, point: Point, nodeRadius: Float, paint: Paint) {
    drawCircle(point.x, point.y, 1.3f * nodeRadius, Palette.BLACK)
    drawCircle(point.x, point.y, nodeRadius, Palette.GRAY)
    if (paint.textSize < nodeRadius)
        drawText(node.toString(), point.x, point.y + baselineOffset, paint)
}

fun Canvas.drawText(text: String, point: Point, paint: Paint) {
    drawText(text, point.x, point.y, paint)
}

fun Canvas.drawHighlight(point: Point, nodeRadius: Float) =
    drawCircle(point.x, point.y, 1.1f * nodeRadius, Palette.HIGHLIGHT)

fun Canvas.drawCell(row: Int, col: Int, cellSize: Dimension, offset: Dimension, paint: Paint) =
    drawRect(col * cellSize.x + offset.x, row * cellSize.y + offset.y, (col + 1) * cellSize.x + offset.x, (row + 1) * cellSize.y + offset.y, paint)