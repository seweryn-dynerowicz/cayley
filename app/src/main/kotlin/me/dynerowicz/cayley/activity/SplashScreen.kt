package me.dynerowicz.cayley.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle

class SplashScreen : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
