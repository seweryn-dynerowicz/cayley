package me.dynerowicz.cayley.activity

import android.animation.AnimatorSet
import android.app.Activity
import android.graphics.Paint
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.view.View
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.algebra.Group
import me.dynerowicz.cayley.diagram.GraphLayout
import me.dynerowicz.cayley.utils.Palette
import me.dynerowicz.cayley.views.CayleyDiagramView
import me.dynerowicz.cayley.views.SymmetryView

abstract class GroupOverviewActivity : Activity() {

    var group: Group = Group.TrivialGroup
        protected set(newGroup) {
            field = newGroup
            layout = GraphLayout.getLayout(group)
            paletteElements = Palette(group.elements, style = Paint.Style.FILL, strokeWidth = 4.0f)
            paletteActions = Palette(group.generators, style = Paint.Style.STROKE, value = 0.5f, strokeWidth = 3.0f)
        }

    lateinit var layout: GraphLayout
        private set

    lateinit var paletteElements: Palette
        private set

    lateinit var paletteActions: Palette
        private set

    init {
        group = Group.TrivialGroup
    }

    abstract var mainView: ConstraintLayout
    abstract var cayleyDiagram: CayleyDiagramView
    abstract var symmetryObject: SymmetryView

    override fun onResume() {
        super.onResume()
        mainView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    fun onTappedElement(elt: Element) {
        val generators = group.getAsGenerators(elt)

        Log.i(TAG, "onTappedElement: $elt [$generators]")

        val cayleyAnimation = AnimatorSet().apply{
            playSequentially(cayleyDiagram.apply(generators))
            duration = 300L / generators.size
        }

        val symmetryAnimation = AnimatorSet().apply {
            playSequentially(symmetryObject.apply(generators))
            duration = 300L / generators.size
        }

        cayleyAnimation.start()
        symmetryAnimation.start()
    }

    companion object {
        private val TAG = GroupOverviewActivity::class.java.simpleName
        val ORDER = "OR"
    }
}