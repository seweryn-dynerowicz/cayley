package me.dynerowicz.cayley.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import me.dynerowicz.cayley.R
import me.dynerowicz.cayley.groups.cyclic.CyclicGroupOverviewActivity
import me.dynerowicz.cayley.groups.dihedral.DihedralGroupOverviewActivity
import me.dynerowicz.cayley.groups.puzzle.rings.RotatingRingGroupOverviewActivity
import me.dynerowicz.cayley.groups.puzzle.switches.SwitchPuzzleGroupOverviewActivity

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        main_view.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    fun onClick(view: View) {
        val intent: Intent?

        when (view.id) {
            R.id.btn_cyclic_group -> {
                intent = Intent(this, CyclicGroupOverviewActivity::class.java)
                intent.putExtra(GroupOverviewActivity.ORDER, 12)
            }
            R.id.btn_dihedral_group -> {
                intent = Intent(this, DihedralGroupOverviewActivity::class.java)
                intent.putExtra(GroupOverviewActivity.ORDER, 6)
            }
            R.id.btn_switch_puzzle_group -> {
                intent = Intent(this, SwitchPuzzleGroupOverviewActivity::class.java)
                intent.putExtra(GroupOverviewActivity.ORDER, 3)
            }
            R.id.btn_rotating_rings_group -> {
                intent = Intent(this, RotatingRingGroupOverviewActivity::class.java)
            }
            else -> {
                Log.e(TAG, "Unknown view clicked. Ignoring.")
                intent = null
            }
        }

        if (intent != null)
            startActivity(intent)
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}