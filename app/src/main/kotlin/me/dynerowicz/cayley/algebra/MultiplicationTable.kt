package me.dynerowicz.cayley.algebra

import android.util.Log
import me.dynerowicz.cayley.extensions.modulo

class MultiplicationTable(group: Group) {

    private val order = group.order
    private val table = Array (order * order) { index ->
        val rowElement = group.elements[index.div(order)]
        val columnElement = group.elements[index.modulo(order)]

        rowElement * columnElement
    }

    fun print() {
        for (row in 0 until order) {
            val sb = StringBuilder()
            for (col in 0 until order)
                sb.append(table[row * order + col])
            Log.v(TAG, sb.toString())
        }
    }

    operator fun get(rowIndex: Int, columnIndex: Int) = table[rowIndex * order + columnIndex]
    operator fun get(index: Int) = table[index]

    companion object {
        private val TAG = MultiplicationTable::class.java.simpleName
    }
}