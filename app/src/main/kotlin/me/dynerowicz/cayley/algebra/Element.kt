package me.dynerowicz.cayley.algebra

abstract class Element {
    // Identifier of the element
    abstract val id: Int
    // Group operation
    abstract operator fun times(that: Element): Element
    // Element inverse
    abstract operator fun not(): Element

    val actingOn: (Element) -> Element = { other -> other * this }

    // Equality
    override fun equals(other: Any?): Boolean {
        other as Element
        return this.id == other.id
    }

    override fun hashCode(): Int = id
    override fun toString(): String = id.toString()
}