package me.dynerowicz.cayley.algebra

import me.dynerowicz.cayley.groups.cyclic.CyclicElement
import me.dynerowicz.cayley.groups.dihedral.DihedralElement
import me.dynerowicz.cayley.groups.puzzle.rings.RotatingRingPuzzleElement
import me.dynerowicz.cayley.groups.puzzle.switches.SwitchPuzzleElement
import me.dynerowicz.cayley.groups.trivial.TrivialElement
import me.dynerowicz.cayley.utils.Generator
import me.dynerowicz.cayley.utils.PrecedenceInfo

sealed class Group(
    val neutral: Element,
    vararg val generators: Element
) {

    val order: Int
    val elements: MutableList<Element> = ArrayList()
    val precedence: MutableMap<Element, PrecedenceInfo> = HashMap()

    init {
        addElement(neutral)

        val processedElements: MutableSet<Element> = HashSet()

        val discoveredElements: MutableList<Element> = ArrayList()
        discoveredElements.add(neutral)

        while (elements.size <= safetyLimit && discoveredElements.isNotEmpty()) {
            val current = discoveredElements.first()
            generators.forEach { generator ->
                val next = generator.actingOn(current)
                if (next !in processedElements && next !in discoveredElements) {
                    discoveredElements.add(next)
                    addElement(next, PrecedenceInfo(current, generator))
                }
            }
            processedElements.add(current)
            discoveredElements.remove(current)
        }

        elements.sortBy { elt -> elt.id }
        order = elements.size
    }

    private fun addElement(current: Element, precedenceInfo: PrecedenceInfo? = null) {
        elements.add(current)
        if (precedenceInfo != null)
            precedence.put(current, precedenceInfo)
    }

    fun getAsGenerators(element: Element) : List<Generator> {
        val generatorList = ArrayList<Generator>()

        var precedenceInfo = precedence[element]
        while (precedenceInfo != null) {
            val(parent, action) = precedenceInfo
            generatorList.add(action)
            precedenceInfo = precedence[parent]
        }

        return generatorList
    }

    object TrivialGroup : Group(neutral = TrivialElement, generators = TrivialElement)

    class CyclicGroup(cycleLength: Int) : Group(
        neutral = CyclicElement(value = 0, cycleLength = cycleLength),
        generators = CyclicElement(value = 1, cycleLength = cycleLength)
    )

    class DihedralGroup(val numberOfSides: Int) : Group(
        neutral = DihedralElement(numberOfSides = numberOfSides),
        generators = *arrayOf(
            DihedralElement(rotation = 1, numberOfSides = numberOfSides),
            DihedralElement(flip = true, numberOfSides = numberOfSides)
        )
    )

    class SwitchPuzzleGroup(val numberOfSwitches: Int) : Group(
        neutral = SwitchPuzzleElement(numberOfSwitches = numberOfSwitches),
        generators = *Array(numberOfSwitches) { index -> SwitchPuzzleElement(pressedSwitch = index, numberOfSwitches = numberOfSwitches) }
    )

    open class RotatingRingPuzzleGroup(val numberOfRings: Int, val numberOfPositions: Int, ringsConnections: Map<Int, Set<Int>>? = null) : Group(
        neutral = RotatingRingPuzzleElement(numberOfRings, numberOfPositions),
        generators = *Array(numberOfRings) { currentRing ->
            val connectedTo = ringsConnections?.get(currentRing)

            val rings = IntArray(numberOfRings) { otherRing ->
                if(otherRing == currentRing || (connectedTo != null && connectedTo.contains(otherRing)))
                    1
                else
                    0
            }

            RotatingRingPuzzleElement(rings = rings, numberOfPositions = numberOfPositions)
        }
    )

    companion object {
        private val safetyLimit = 100
    }
}