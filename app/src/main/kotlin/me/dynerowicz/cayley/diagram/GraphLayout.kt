package me.dynerowicz.cayley.diagram

import android.graphics.Path
import android.util.Log
import me.dynerowicz.cayley.algebra.Element
import me.dynerowicz.cayley.algebra.Group
import me.dynerowicz.cayley.algebra.Group.*
import me.dynerowicz.cayley.groups.cyclic.CyclicGroupLayout
import me.dynerowicz.cayley.groups.dihedral.DihedralGroupLayout
import me.dynerowicz.cayley.groups.puzzle.rings.RotatingRingGroupLayout
import me.dynerowicz.cayley.groups.puzzle.switches.SwitchPuzzleGroupLayout
import me.dynerowicz.cayley.groups.trivial.TrivialGroupLayout
import me.dynerowicz.cayley.utils.Generator
import me.dynerowicz.cayley.utils.Point

abstract class GraphLayout(protected val group: Group) {

    var nodeRadius: Float = 0.0f
        protected set

    val nodesCoordinates: MutableMap<Element, Point> = HashMap()
    val drawnPaths: MutableMap<Generator, MutableList<Path>> = HashMap()

    protected val animationPaths: MutableMap<Arrow, Path> = HashMap()

    init {
        group.generators.forEach { generator -> drawnPaths[generator] = ArrayList() }

        group.elements.forEach { element ->
            nodesCoordinates[element] = Point()
            group.generators.forEach { generator ->
                val path = Path()
                val arrow = Arrow(element, generator.actingOn(element), generator)
                animationPaths[arrow] = path
                if (drawPathCondition(arrow))
                    drawnPaths[generator]?.add(path)
            }
        }
    }

    fun getAnimationPath(source: Element, target: Element, action: Generator) = animationPaths[Arrow(source, target, action)]

    open fun drawPathCondition(arrow: Arrow): Boolean = true

    open fun onSizeChanged(height: Int, width: Int) {
        nodeRadius = 0.0375f * Math.min(height, width)
        Log.i(TAG, "OnSizeChanged($height, $width) : $nodeRadius")
    }

    override fun toString(): String =
        StringBuilder().apply {
            append("nodes=${nodesCoordinates.size}, arrows=${animationPaths.size}, drawn=")
            if (drawnPaths.isNotEmpty())
                drawnPaths.forEach { (generator, paths) -> append("$generator=${paths.size} ") }
            else
                append("none")
        }.toString()

    data class Arrow(val source: Element, val target: Element, val generator: Generator)

    companion object {
        private val TAG = GraphLayout::class.java.simpleName

        fun getLayout(group: Group): GraphLayout =
            when(group) {
                is TrivialGroup -> TrivialGroupLayout(group)
                is CyclicGroup -> CyclicGroupLayout(group)
                is DihedralGroup -> DihedralGroupLayout(group)
                is SwitchPuzzleGroup -> SwitchPuzzleGroupLayout(group)
                is RotatingRingPuzzleGroup -> RotatingRingGroupLayout(group)
            }
    }
}