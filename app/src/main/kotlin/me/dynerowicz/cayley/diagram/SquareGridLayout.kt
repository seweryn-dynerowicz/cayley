package me.dynerowicz.cayley.diagram

import android.util.Log
import me.dynerowicz.cayley.algebra.Group
import me.dynerowicz.cayley.extensions.Dimension
import me.dynerowicz.cayley.extensions.modulo
import me.dynerowicz.cayley.extensions.setEdge

class SquareGridLayout(group: Group) : GraphLayout(group) {

    private val numberOfColumns: Int = Math.sqrt(group.order.toDouble()).toInt() + 1
    private val numberOfRows: Int = group.order.div(numberOfColumns) + 1

    private var nodeSpacing = Dimension()

    override fun onSizeChanged(height: Int, width: Int) {
        super.onSizeChanged(height, width)

        nodeSpacing.set(width / numberOfColumns, height / numberOfRows)

        nodesCoordinates.forEach { (node, point) ->
            val colId = node.id.div(numberOfColumns)
            val rowId = node.id.modulo(numberOfColumns)

            point.set((rowId + 0.5f) * nodeSpacing.x, (colId + 0.5f) * nodeSpacing.y)

            Log.d(TAG, "Node: $node@$point")
        }

        animationPaths.forEach { (arrow, path) ->
            Log.d(TAG, "Arrow: ${arrow.source.id} -> ${arrow.target.id} [${arrow.generator.id}]")
            val source = nodesCoordinates[arrow.source]
            val target = nodesCoordinates[arrow.target]
            path.setEdge(source, target)
        }
    }

    companion object {
        private val TAG = SquareGridLayout::class.java.simpleName
    }
}