package me.dynerowicz.cayley

import me.dynerowicz.cayley.algebra.Group.DihedralGroup
import me.dynerowicz.cayley.groups.dihedral.DihedralElement
import me.dynerowicz.cayley.utils.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class DihedralGroupUnitTest {
    private val numberOfSides = 4
    lateinit var group: DihedralGroup

    @Before
    fun setUp() {
        group = DihedralGroup(numberOfSides)
    }

    @Test
    fun hasTwiceAsManyElements() = Assert.assertEquals(group.elements.size, 2 * numberOfSides)

    @Test
    fun groupIsClosed() = Assert.assertTrue(group.isClosed())

    @Test
    fun groupIsAssociative() = Assert.assertTrue(group.isAssociative())

    @Test
    fun groupHasNeutral() = Assert.assertTrue(group.hasNeutral())

    @Test
    fun groupHasInverses() = Assert.assertTrue(group.hasInverses())

    @Test
    fun groupHasValidInversion() = Assert.assertTrue(group.hasValidInversion())

    @Test
    fun groupIsCommutative() = Assert.assertTrue(group.isCommutative())

    @Test
    fun groupHasValidDecomposition() {
        println(group.precedenceGraph())
        Assert.assertTrue(group.hasValidDecomposition())
    }

    @Test
    fun groupHasProperIdentifiers() = Assert.assertTrue(group.hasProperIdentifiers())

    @Test
    fun groupHasValidDihedralRelators() {
        val f = DihedralElement(flip = true, numberOfSides = numberOfSides)
        val r = DihedralElement(rotation = 1, numberOfSides = numberOfSides)

        var result = group.neutral
        for (power in 1 .. numberOfSides)
            result = r * result

        Assert.assertEquals("e != f * f", group.neutral, f * f)
        Assert.assertEquals("e != r^n", group.neutral, result)
        Assert.assertEquals("f != r * f * r", f, r * f * r)
    }
}