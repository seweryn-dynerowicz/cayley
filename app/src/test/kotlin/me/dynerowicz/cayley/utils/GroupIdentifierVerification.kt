package me.dynerowicz.cayley.utils

import me.dynerowicz.cayley.algebra.Group

fun Group.hasProperIdentifiers() : Boolean {
    val identifierSet: MutableSet<Int> = HashSet()

    elements.forEach { element ->
        when (element.id) {
            in 0 until order -> identifierSet.add(element.id)
            else -> println("Identifier consistency violation : ${element.id}")
        }
    }

    if (identifierSet.size != order)
        println("Missing identifiers !")

    return identifierSet.size == order
}