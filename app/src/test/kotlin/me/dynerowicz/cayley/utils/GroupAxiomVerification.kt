package me.dynerowicz.cayley.utils

import me.dynerowicz.cayley.algebra.Group

inline fun <T> Iterable<T>.forAll(predicate: (T) -> Boolean) = all(predicate)

fun Group.isClosed(): Boolean =
    elements.forAll { elt1 ->
        elements.forAll { elt2 ->
            val result = elt1 * elt2
            if (result !in elements)
                println("Closure violation : $elt1 * $elt2 != $result")
            result in elements
        }
    }

fun Group.isAssociative(): Boolean =
    elements.forAll { a ->
        elements.forAll { b ->
            elements.forAll { c ->
                val left = a * (b * c)
                val right = (a * b) * c
                if (left != right)
                    println("Associativity violation : $left = $a * ($b * $c) != ($a * $b) * $c = $right")
                left == right
            }
        }
    }

fun Group.hasNeutral() : Boolean {
    return elements.forAll { elt ->
        val isNeutralFor = (elt == neutral * elt) && (elt == elt * neutral)

        if (!isNeutralFor)
            println("Neutrality violation : $elt")

        isNeutralFor
    }
}

fun Group.hasInverses() : Boolean =
    elements.forAll { elt ->
        val inverseFound = elements.any { candidate ->
            neutral == (elt * candidate) && neutral == (candidate * elt)
        }

        if (!inverseFound)
            println("Inverse violation : $elt")

        inverseFound
    }

fun Group.hasValidInversion() : Boolean =
    elements.forAll { elt ->
        val validInversion = (neutral == elt * !elt) && (neutral == !elt * elt)

        if (!validInversion)
            println("Inversion violation : $elt")

        validInversion
    }

fun Group.isCommutative() : Boolean =
    elements.forAll { elt1 ->
        elements.forAll { elt2 ->
            val elementsCommute = elt1 * elt2 == elt2 * elt1

            if (!elementsCommute)
                println("Commutativity violation : $elt1 * $elt2 = ${elt1 * elt2} != ${elt2 * elt1} = $elt2 * $elt1")

            elementsCommute
        }
    }

fun Group.hasValidDecomposition() : Boolean =
    elements.forAll { element ->
        val asGenerators = getAsGenerators(element)

        var result = neutral
        asGenerators.reversed().forEach { generator -> result = generator.actingOn(result) }

        val validDecomposition = asGenerators.all { generator -> generator in generators } && element == result

        if (!validDecomposition)
            println("Invalid decomposition of $element into $asGenerators")

        validDecomposition
    }