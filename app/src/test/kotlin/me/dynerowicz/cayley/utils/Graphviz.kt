package me.dynerowicz.cayley.utils

import me.dynerowicz.cayley.algebra.Group

fun Group.precedenceGraph() : String =
    StringBuilder("digraph PREC {\n").apply {
        precedence.forEach { element, (parent, _) ->
            append("  $parent -> $element\n")
        }
    }.append('}').toString()

fun Group.cayleyGraph() : String =
    StringBuilder("digraph CAYLEY {\n").apply {
        elements.forEach { element ->
            generators.forEach { generator ->
                append("  $element -> ${generator.actingOn(element)}\n")
            }
        }
    }.append('}').toString()