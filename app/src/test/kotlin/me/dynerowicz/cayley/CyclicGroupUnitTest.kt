package me.dynerowicz.cayley

import me.dynerowicz.cayley.algebra.Group.CyclicGroup
import me.dynerowicz.cayley.groups.cyclic.CyclicElement
import me.dynerowicz.cayley.utils.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CyclicGroupUnitTest {

    private val order: Int = 10
    lateinit private var group: CyclicGroup

    @Before
    fun setUp() {
        group = CyclicGroup(order)
    }

    @Test
    fun cyclicGroupSizeEqualsOrder() = Assert.assertEquals(group.order, order)

    @Test
    fun groupIsClosed() = Assert.assertTrue(group.isClosed())

    @Test
    fun groupIsAssociative() = Assert.assertTrue(group.isAssociative())

    @Test
    fun groupHasNeutral() = Assert.assertTrue(group.hasNeutral())

    @Test
    fun groupHasInverses() = Assert.assertTrue(group.hasInverses())

    @Test
    fun groupHasValidInversion() = Assert.assertTrue(group.hasValidInversion())

    @Test
    fun groupIsCommutative() = Assert.assertTrue(group.isCommutative())

    @Test
    fun groupHasValidDecomposition() = Assert.assertTrue(group.hasValidDecomposition())

    @Test
    fun groupHasProperIdentifiers() = Assert.assertTrue(group.hasProperIdentifiers())

    @Test
    // Test that r^n = e with multiplication by r on the right
    fun groupHasValidCyclicRelator() {
        val r = CyclicElement(value = 1, cycleLength = order)
        var result = group.neutral
        for (power in 1 .. order)
            result = r * result
        Assert.assertEquals("e != r^n", group.neutral, result)
    }
}