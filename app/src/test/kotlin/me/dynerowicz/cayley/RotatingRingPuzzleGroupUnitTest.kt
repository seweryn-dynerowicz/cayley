package me.dynerowicz.cayley

import me.dynerowicz.cayley.algebra.Group.RotatingRingPuzzleGroup
import me.dynerowicz.cayley.utils.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class RotatingRingPuzzleGroupUnitTest {

    private val numberOfRings: Int = 3
    private val numberOfPositions: Int = 3
    lateinit private var group: RotatingRingPuzzleGroup

    @Before
    fun setUp() {
        group = RotatingRingPuzzleGroup(numberOfRings = numberOfRings, numberOfPositions = numberOfPositions)
    }

    @Test
    fun groupInformation() {
        println("Group order = ${group.order}")
        group.generators.forEach { println(it) }

        println(group.precedenceGraph())
        println(group.cayleyGraph())
    }

    @Test
    fun groupIsClosed() = Assert.assertTrue(group.isClosed())

    @Test
    fun groupIsAssociative() = Assert.assertTrue(group.isAssociative())

    @Test
    fun groupHasNeutral() = Assert.assertTrue(group.hasNeutral())

    @Test
    fun groupHasInverses() = Assert.assertTrue(group.hasInverses())

    @Test
    fun groupHasValidInversion() = Assert.assertTrue(group.hasValidInversion())

    @Test
    fun groupIsCommutative() = Assert.assertTrue(group.isCommutative())

    @Test
    fun groupHasValidDecomposition() = Assert.assertTrue(group.hasValidDecomposition())

    @Test
    fun groupHasProperIdentifiers() = Assert.assertTrue(group.hasProperIdentifiers())
}